<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return view('layouts/main');
})->name('home');

Route::get('about', function () {
    return view('layouts/about');
})->name('index.php/about');

Route::get('team', function () {
    return view('layouts/team');
})->name('index.php/team');

Route::get('service', function () {
    return view('layouts/service');
})->name('index.php/service');

Route::get('realisation', function () {
    return view('layouts/realisation');
})->name('index.php/realisation');

Route::get('contact', function () {
    return view('layouts/contact');
})->name('index.php/contact');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
