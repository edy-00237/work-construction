<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{!! asset('assets/images/fav.png') !!}">
    <title>WORK Construction</title>

    <!-- fontawesome css -->
    <link rel="stylesheet" href="{!! asset('assets/css/plugins/fontawesome-5.css') !!}">
    <!-- fontawesome css -->
    <link rel="stylesheet" href="{!! asset('assets/css/plugins/swiper.css') !!} ">
    <link rel="stylesheet" href="{!! asset('assets/css/plugins/aos.css') !!} ">
    <link rel="stylesheet" href="{!! asset('assets/css/plugins/unicons.css') !!} ">
    <link rel="stylesheet" href="{!! asset('assets/css/plugins/metismenu.css') !!} ">
    <link rel="stylesheet" href="{!! asset('assets/css/plugins/hover-revel.css') !!} ">
    <link rel="stylesheet" href="{!! asset('assets/css/plugins/timepickers.min.css') !!} ">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="{!! asset('assets/css/vendor/bootstrap.min.css') !!} ">
    <!-- main css -->
    <link rel="stylesheet" href="{!! asset('assets/css/style.css') !!} ">
</head>

<body class="home-one">

<!-- header style two -->
<!-- header srea start -->
<header class="header-one">
    <!-- hedaer top -->
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header-top-wrapper">
                        <!-- left area header top -->
                        <div class="left">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="13" height="17" viewBox="0 0 13 17" fill="none">
                                    <path d="M6.95455 6.95454H12.3636L5.40909 17V10.0455H0L6.95455 -3.8147e-06V6.95454Z" fill="#1875ba" />
                                </svg>
                            </div>
                            <p class="disc">
                                Nous passerons en revue toutes les étapes de la construction
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- hedaer bottom -->
    <!-- header style hear -->
    <div class="header-mid">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header-mid-one-wrapper">
                        <div class="logo-area">
                            <a href="index-2.html">
                                <img src="{!! asset('assets/images/logo/01.png') !!}" height="60px" width="60px" alt="logo">
                            </a>
                        </div>
                        <div class="header-right">
                            <!-- sinle map area -->
                            <div class="single-component info-con">
                                <div class="icon">
                                    <i class="fal fa-map-marker-alt"></i>
                                </div>
                                <div class="info">
                                    <span>Notre localisation</span>
                                    <a href="#">Yaoundé | Fougerolles Entrée Amity</a>
                                </div>
                            </div>
                            <!-- sinle map area -->
                            <!-- sinle map area -->
                            <div class="single-component info-con">
                                <div class="icon">
                                    <i class="fal fa-envelope"></i>
                                </div>
                                <div class="info">
                                    <span>Envoyez-nous un courrier</span>
                                    <a href="mailto:name@email.com">infos@works-construction.com</a>
                                </div>
                            </div>
                            <!-- sinle map area -->
                            <!-- sinle map area -->
                            <!-- sinle map area -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- header style hearm end -->

    <div class="nav-area-one  header--sticky">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="nav-header-area">
                        <div class="clip-path"></div>
                        <div class="wrapper">
                            <!-- header style two -->
                            <!-- nav area start -->
                            <div class="main-nav-desk nav-area">
                                <nav>
                                    <ul>
                                        <li class="menu-item">
                                            <a class="nav-item" href="{{ route('home') }}">Accueil</a>
                                        </li>
                                        <li class="has-droupdown pages">
                                            <a class="nav-link" href="#">Qui sommes-nous</a>
                                            <ul class="submenu inner-page">
                                                <li><a href="{{ route('index.php/about') }}">A propos de nous</a></li>
                                                <li><a href="{{ route('index.php/team') }}">Notre equipe</a></li>
                                                <li><a href="#">Chiffre d'affaire</a></li>
                                                <li><a href="#">Nos partenaires</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item">
                                            <a class="nav-item" href="{{ route('index.php/realisation') }}">
                                                Nos expériences
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a class="nav-item" href="{{ route('index.php/service') }}">Nos Services</a>
                                        </li>
                                        <li class="menu-item">
                                            <a class="nav-item" href="{{ route('index.php/contact')">Contactez-nous</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <!-- nav-area end -->
                            <!-- header style two End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header srea end -->

<!-- side bar for desktop -->
<div id="side-bar" class="side-bar">
    <button class="close-icon-menu"><i class="far fa-times"></i></button>
    <!-- inner menu area desktop start -->
    <div class="inner-main-wrapper-desk d-none d-lg-block">
        <div class="thumbnail">
            <img src="{!! asset('assets/images/banner/04.jpg') !!}" alt="elevate">
        </div>
        <div class="inner-content">
            <h4 class="title">Nous construisons des bâtiments et de grandes maisons constructives.</h4>
            <p class="disc">
                Nous maîtrisons des tâches plus ou moins complexes, nous offrons des garanties à long terme et nous maîtrisons régulièrement les nouvelles technologies..
            </p>
            <div class="footer">
                <h4 class="title">Vous avez un projet en tête ?</h4>
                <a href="contact.html" class="rts-btn btn-seconday">Parlons-en</a>
            </div>
        </div>
    </div>

    <!-- mobile menu area start -->
    <div class="mobile-menu d-block d-xl-none">
        <nav class="nav-main mainmenu-nav mt--30">
            <ul class="mainmenu" id="mobile-menu-active">
                <li class="has-droupdown">
                    <a href="#" class="main">Home</a>
                    <ul class="submenu">
                        <a href="#" class="tag">Multipage</a>
                        <li><a class="mobile-menu-link" href="index-2.html">Main Construction</a></li>
                        <li><a class="mobile-menu-link" href="index-two.html">Construction Home</a></li>
                        <li><a class="mobile-menu-link" href="index-three.html">Renovation Home</a></li>
                        <li><a class="mobile-menu-link" href="index-four.html">Construction Home</a></li>
                        <li><a class="mobile-menu-link" href="index-five.html">Construction Home</a></li>
                        <li><a class="mobile-menu-link" href="index-six.html">Renovation Home</a></li>
                        <li><a class="mobile-menu-link" href="index-seven.html">Engineering Home</a></li>
                        <li><a class="mobile-menu-link" href="index-eight.html">Handyman Home</a></li>
                        <li><a class="mobile-menu-link" href="index-nine.html">Industrial Home</a></li>
                    </ul>
                    <ul class="submenu">
                        <a href="#" class="tag">Onepage</a>
                        <li><a class="mobile-menu-link" href="onepage-one.html">Main Construction Onepage</a></li>
                        <li><a class="mobile-menu-link" href="onepage-two.html">Construction Onepage</a></li>
                        <li><a class="mobile-menu-link" href="onepage-three.html">Renovation Onepage</a></li>
                        <li><a class="mobile-menu-link" href="onepage-four.html">Construction Onepage</a></li>
                        <li><a class="mobile-menu-link" href="onepage-five.html">Construction Onepage</a></li>
                        <li><a class="mobile-menu-link" href="onepage-six.html">Renovation Onepage</a></li>
                        <li><a class="mobile-menu-link" href="onepage-eight.html">Handyman Onepage</a></li>
                        <li><a class="mobile-menu-link" href="onepage-seven.html">Engineering Onepage</a></li>
                        <li><a class="mobile-menu-link" href="onepage-nine.html">Industrial Onepage</a></li>
                    </ul>
                </li>
                <li class="has-droupdown">
                    <a href="#" class="main">Pages</a>
                    <ul class="submenu">
                        <li class="has-droupdown third-lvl">
                            <a class="main" href="#">Who We Are</a>
                            <ul class="submenu-third-lvl">
                                <li><a href="about.html"></a>About Us</li>
                                <li><a href="vision.html"></a>Vision</li>
                                <li><a href="careers.html"></a>Careers</li>
                                <li><a href="safety.html"></a>Safety</li>
                                <li><a href="sustainability.html"></a>Sustainability</li>
                            </ul>
                        </li>
                        <li><a class="mobile-menu-link" href="company-story.html">Our History</a></li>
                        <li><a class="mobile-menu-link" href="team.html">Team</a></li>
                        <li><a class="mobile-menu-link" href="team-details.html">Team Details</a></li>
                        <li><a class="mobile-menu-link" href="appoinment.html">Appoinment</a></li>
                        <li><a class="mobile-menu-link" href="404.html">Error 404</a></li>
                        <li class="has-droupdown third-lvl">
                            <a class="main" href="#">Shop</a>
                            <ul class="submenu-third-lvl">
                                <li><a href="shop.html"></a>Shop</li>
                                <li><a href="single-product.html"></a>Single Product</li>
                                <li><a href="cart.html"></a>Cart</li>
                                <li><a href="checkout.html"></a>Checkout</li>
                                <li><a href="account.html"></a>Account</li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="has-droupdown">
                    <a href="#" class="main">Services</a>
                    <ul class="submenu">
                        <li><a class="mobile-menu-link" href="service.html">Service</a></li>
                        <li><a class="mobile-menu-link" href="service-details.html">Service Details</a></li>
                    </ul>
                </li>
                <li class="has-droupdown">
                    <a href="#" class="main">Portfolio</a>
                    <ul class="submenu">
                        <li><a class="mobile-menu-link" href="project.html">Project</a></li>
                        <li><a class="mobile-menu-link" href="project-details.html">Project Details</a></li>
                    </ul>
                </li>
                <li class="has-droupdown">
                    <a href="#" class="main">Blog</a>
                    <ul class="submenu">
                        <li><a class="mobile-menu-link" href="blog-list.html">Blog List</a></li>
                        <li><a class="mobile-menu-link" href="blog-grid.html">Blog Grid</a></li>
                        <li><a class="mobile-menu-link" href="blog-details.html">Blog Details</a></li>
                    </ul>
                </li>
                <li>
                    <a href="contactus.html" class="main">Contact Us</a>
                </li>
            </ul>
        </nav>

        <div class="social-wrapper-one">
            <ul>
                <li>
                    <a href="#">
                        <i class="fa-brands fa-facebook-f"></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa-brands fa-twitter"></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa-brands fa-youtube"></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa-brands fa-linkedin-in"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- mobile menu area end -->
</div>
<!-- header style two End -->

<!-- banner start -->
<div class="rts-banner-main-area-swiper">
    <!-- Slider main container -->
    <div class="swiper-container">
        <!-- Additional required wrapper -->

        <div class="swiper-pagination"></div>

        <div class="swiper-wrapper">
            <!-- Slides -->
            <div class="swiper-slide">
                <!-- rts-banner area end-->
                <div class="rts-section-gap ptb_sm-20 rts-banner-one bg_image bg_image--1">
                    <div class="banner-shape-area">
                        <img src="{!! asset('assets/images/banner/shape/01.png') !!} " alt="banner-shape" class="shape shape-1">
                        <img src="{!! asset('assets/images/banner/shape/02.png') !!} " alt="banner-shape" class="shape shape-2">
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="banner-one-wrapper ptb--110">
                                        <span class="b_one-pre">
                                            Bienvenue sur works construction
                                        </span>
                                    <h1 class="title-banner">
                                        Nous fournissons
                                        <br>
                                        des solutions à la pointe
                                        <br>
                                        de l'industrie
                                    </h1>
                                    <div class="button-area-banner">
                                        <a href="#" class="rts-btn btn-primary"> Démarrer </a>
                                        <!-- call now information -->
                                        <div class="call-area">
                                            <div class="icon">
                                                <i class="fa-regular fa-phone-volume"></i>
                                            </div>
                                            <div class="detail">
                                                <span>Appelez-nous</span>
                                                <a href="" class="number">
                                                    237 697 07 33 58 / 237 673 25 81 79
                                                </a>
                                            </div>
                                        </div>
                                        <!-- call now information end -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- rts-banner area end -->
            </div>
            <div class="swiper-slide">
                <!-- rts-banner area end-->
                <div class="rts-section-gap ptb_sm-20 rts-banner-one bg_image bg_image--2">
                    <div class="banner-shape-area">
                        <img src="{!! asset('assets/images/banner/shape/01.png') !!}" alt="banner-shape" class="shape shape-1">
                        <img src="{!! asset('assets/images/banner/shape/02.png') !!}" alt="banner-shape" class="shape shape-2">
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="banner-one-wrapper ptb--110">
                                        <span class="b_one-pre">
                                           Bienvenue sur works construction
                                        </span>
                                    <h1 class="title-banner">
                                        Nous fournissons
                                        <br>
                                        des solutions à la pointe
                                        <br>
                                        de l'industrie
                                    </h1>
                                    <div class="button-area-banner">
                                        <a href="#" class="rts-btn btn-primary">Démarrer</a>
                                        <!-- call now information -->
                                        <div class="call-area">
                                            <div class="icon">
                                                <i class="fa-regular fa-phone-volume"></i>
                                            </div>
                                            <div class="detail">
                                                <span>Appelez-nous</span>
                                                <a href="tel:+4733378901" class="number">
                                                    237 697 07 33 58 / 237 673 25 81 79
                                                </a>
                                            </div>
                                        </div>
                                        <!-- call now information end -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- rts-banner area end -->
            </div>
            <div class="swiper-slide">
                <!-- rts-banner area end-->
                <div class="rts-section-gap ptb_sm-20 rts-banner-one bg_image bg_image--3">
                    <div class="banner-shape-area">
                        <img src="{!! asset('assets/images/banner/shape/01.png') !!}" alt="banner-shape" class="shape shape-1">
                        <img src="{!! asset('assets/images/banner/shape/02.png') !!}" alt="banner-shape" class="shape shape-2">
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="banner-one-wrapper ptb--110">
                                        <span class="b_one-pre">
                                            Bienvenue sur works construction
                                        </span>
                                    <h1 class="title-banner">
                                        Nous fournissons
                                        <br>
                                        des solutions à la pointe
                                        <br>
                                        de l'industrie
                                    </h1>
                                    <div class="button-area-banner">
                                        <a href="#" class="rts-btn btn-primary">Démarrer</a>
                                        <!-- call now information -->
                                        <div class="call-area">
                                            <div class="icon">
                                                <i class="fa-regular fa-phone-volume"></i>
                                            </div>
                                            <div class="detail">
                                                <span>Appelez-nous</span>
                                                <a href="" class="number">
                                                    237 697 07 33 58 / 237 673 25 81 79
                                                </a>
                                            </div>
                                        </div>
                                        <!-- call now information end -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- rts-banner area end -->
            </div>
            <!-- If we need pagination -->
        </div>
        <!-- If we need navigation buttons -->
    </div>
</div>
<!-- banner end -->

<!-- latest projects start -->
<div class="rts-about-area-about rts-section-gap">
{{--    <div class="row">--}}
{{--        <div class="col-lg-12">--}}
{{--            <div class="elecate-center-title" data-sal="slide-up" data-sal-delay="150" data-sal-duration="800">--}}
{{--                <span class="title">ACCEUIL</span>--}}
{{--                <h3 class="title"></h3>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="container pb--45 plr_sm--15">
        <div class="row">
            <div class="col-lg-12 about-a-p-cont">
                <div class="about-inner-wrapper-inner">
                    <div class="title-three-left">
                        <h3 class="title animated fadeIn sal-animate" data-sal="slide-up" data-sal-delay="100" data-sal-duration="800">
                            Mot de bienvenue du directeur Général
                            <br>
                        </h3>
                    </div>
                    <div class="main-content-area-about-p">
                        <p class="disc">
                            Mesieurs, Mesdames

                            C'est avec une immense fierté et une grande émotion que je vous souhaite la bienvenue au sein de notre entreprise. En tant que Directeur Général de WorkConstruction, je suis honoré de diriger une équipe de professionnels passionnés et dévoués qui incarnent notre vision et nos valeurs.
                            <br>

                            WorkConstruction n'est pas seulement une entreprise de construction, c'est une communauté d'artisans, d'ingénieurs, d'architectes et de visionnaires qui œuvrent ensemble pour transformer des rêves en réalité. Depuis nos modestes débuts, nous avons bâti une réputation d'excellence, d'innovation et d'intégrité. Aujourd'hui, nous sommes reconnus pour notre capacité à relever les défis les plus complexes et à créer des espaces qui transcendent les attentes. <br>

                            Notre engagement envers la qualité, la sécurité et la durabilité est au cœur de tout ce que nous entreprenons. Nous investissons continuellement dans la formation, la technologie et les pratiques de pointe pour garantir que chaque projet que nous entreprenons est une œuvre d'art, reflétant nos normes les plus élevées. <br>

                            Chez WorkConstruction, nous croyons que chaque brique posée, chaque poutre assemblée et chaque sourire satisfait de nos clients contribuent à la construction d'un avenir meilleur. Nous sommes fiers de contribuer au développement de collectivités florissantes, de bâtiments emblématiques et de lieux de vie inspirants. <br>

                            En tant que famille WorkConstruction, nous nous soutenons mutuellement, nous partageons des idées novatrices et nous collaborons pour créer des réalisations qui perdureront dans le temps. Je vous encourage à apporter votre passion, votre expertise et votre vision unique à notre entreprise. <br>

                            Que ce voyage au sein de WorkConstruction soit rempli de découvertes, de réalisations gratifiantes et de succès partagés. Ensemble, nous construirons un avenir exceptionnel. <br>

                            Avec gratitude et enthousiasme,<br>

                            Aimée Sésaire TCHOUNGA <br>
                            Directeur Général <br>
                            WorkConstruction
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-text">
            <h2 class="title">Works Construction</h2>
        </div>
    </div>
</div>
<div class="goals-number-section-start pb--90">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="title-mid-wrapper-home-two left">
                    <span class="pre">Notre histoire Vue d'ensemble</span>
                    <h2 class="title">Nos objectifs en chiffres</h2>
                </div>
            </div>
        </div>
        <div class="row mt--20">

            <section class="h--timeline js-h--timeline h--timeline--loaded">
                <div class="h--timeline-container">
                    <div class="h--timeline-dates">
                        <div class="h--timeline-line" style="width: 2625px;">
                            <ol>
                                <li><a href="javascript:void(0);" data-date="16/01/2014" class="h--timeline-date h--timeline-date--older-event" style="left:120px">2023</a></li>
                                <li><a href="javascript:void(0);" data-date="28/02/2014" class="h--timeline-date h--timeline-date--older-event" style="left:375px">2022</a></li>
                                <li><a href="javascript:void(0);" data-date="20/05/2014" class="h--timeline-date h--timeline-date--selected" style="left:870px">2020</a></li>
                            </ol>

                            <span class="h--timeline-filling-line" aria-hidden="true" style="transform: scaleX(0.340815);"></span>
                        </div> <!-- .h-timeline-line -->
                    </div> <!-- .h-timeline-dates -->

                    <nav class="h--timeline-navigation-container">
                        <ul>
                            <li><a href="javascript:void(0);" class="text-replace h--timeline-navigation h--timeline-navigation--prev h--timeline-navigation--inactive">Prev</a></li>
                            <li><a href="javascript:void(0);" class="text-replace h--timeline-navigation h--timeline-navigation--next">Next</a></li>
                        </ul>
                    </nav>
                </div> <!-- .h-timeline-container -->

                <div class="h--timeline-events">
                    <ol>
                        <li class="h--timeline-event text-component">
                            <div class="h--timeline-event-content container">
                                <div class="row mt--0">
                                    <div class="col-lg-8">
                                        <h5 class="title-goal-doller">
                                            116 millions fcfa
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li class="h--timeline-event text-component">
                            <div class="h--timeline-event-content container">
                                <div class="row mt--0">
                                    <div class="col-lg-10">
                                        <h5 class="title-goal-doller">
                                            60 millions fcfa
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="h--timeline-event text-component h--timeline-event--selected">
                            <div class="h--timeline-event-content container">
                                <div class="row mt--0">
                                    <div class="col-lg-10">
                                        <h5 class="title-goal-doller">
                                            25 millions fcfa
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ol>
                </div> <!-- .h-timeline-events -->
            </section>
        </div>
    </div>
</div>

<div class="rts-working-process-area rts-section-gap home-seven">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="heading-title">
                    <div class="elecate-center-title sal-animate" data-sal="slide-up" data-sal-delay="150" data-sal-duration="800">
                        <span>Processus de travail</span>
                        <h3 class="title animated fadeIn">Suivre 4 étapes de travail faciles</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="working-process-inner">
            <div class="row justify-content-center">
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="working-wrapper">
                        <div class="icon"></div>
                        <div class="content">
                            <h3 class="main-title animated fadeIn">Traitement des projets</h3>
                            <p class="desc">Cursus euismod dictumst a non dis nisi sociosqu mauris.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="working-wrapper">
                        <div class="icon two"></div>
                        <div class="content">
                            <h3 class="main-title animated fadeIn">Produits de haute qualité</h3>
                            <p class="desc">Cursus euismod dictumst a non dis nisi sociosqu mauris.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="working-wrapper">
                        <div class="icon three"></div>
                        <div class="content">
                            <h3 class="main-title animated fadeIn">Produits de choix</h3>
                            <p class="desc">Cursus euismod dictumst a non dis nisi sociosqu mauris.</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="working-wrapper">
                        <div class="icon four"></div>
                        <div class="content">
                            <h3 class="main-title animated fadeIn">Qualité Fini</h3>
                            <p class="desc">Cursus euismod dictumst a non dis nisi sociosqu mauris.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- latest projects end -->

<!-- rts footer area start -->
<div class="rts-footer-area footer-one footer-bg bg_image">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- footer top -->
                <div class="footer-top-wrapper  ptb--80">
                    <a href="#" class="logo-area" data-sal="zoom-in" data-sal-delay="150" data-sal-duration="800">
                        <img src="{!! asset('assets/images/logo/01.png') !!}" height="60px" width="60px" alt="blog-images">
                    </a>
                    <h4 class="title" data-sal="zoom-in" data-sal-delay="150" data-sal-duration="800">
                        INSCRIVEZ-VOUS À NOTRE LETTRE
                        <br>
                        D'INFORMATION POUR LES DERNIÈRES MISES À JOUR
                    </h4>
                    <div class="subscribe-area" data-sal="zoom-in" data-sal-delay="150" data-sal-duration="800">
                        <form>
                            <input type="email" name="email" placeholder="Email Address" required>
                            <button class="rts-btn btn-primary">S'abonner</button>
                        </form>
                    </div>
                </div>
                <!-- footer top end -->
            </div>
        </div>
    </div>
    <div class="copyright-area-one">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="wrapepr">
                        <p>Copyright 2023 works construction. Tous droits réservés.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- rts footer area end -->

<!-- header style two -->

<!-- progress area start -->
<div class="progress-wrap">
    <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
        <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" style="transition: stroke-dashoffset 10ms linear 0s; stroke-dasharray: 307.919, 307.919; stroke-dashoffset: 307.919;"></path>
    </svg>
</div>
<!-- progress area end -->

<div class="search-input-area">
    <div class="container">
        <div class="search-input-inner">
            <div class="input-div">
                <input id="searchInput1" class="search-input" type="text" placeholder="Search by keyword or #">
                <button><i class="far fa-search"></i></button>
            </div>
        </div>
    </div>
    <div id="close" class="search-close-icon"><i class="far fa-times"></i></div>
</div>


<div id="anywhere-home" class="">
</div>

<!-- pre loader start -->
<div id="elevate-load">
    <div class="loader-wrapper">
        <div class="lds-ellipsis">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
</div>
<!-- pre loader end -->


<!-- jquery js -->
<script src="{!! asset('assets/js/plugins/jquery.min.js') !!}"></script>
<!-- jquery ui -->
<script src="{!! asset('assets/js/vendor/jqueryui.js') !!}"></script>
<!-- counter up -->
<script src="{!! asset('assets/js/plugins/counter-up.js') !!}"></script>
<script src="{!! asset('assets/js/plugins/swiper.js') !!}"></script>
<!-- twinmax -->
<script src="{!! asset('assets/js/vendor/twinmax.js') !!}"></script>
<!-- split text js -->
<script src="{!! asset('assets/js/vendor/split-text.js') !!}"></script>
<!-- text plugins -->
<script src="{!! asset('assets/js/plugins/text-plugins.js') !!}"></script>
<!-- metismenu js -->
<script src="{!! asset('assets/js/plugins/metismenu.js') !!}"></script>
<!-- waypoint js -->
<script src="{!! asset('assets/js/vendor/waypoint.js') !!}"></script>
<!-- waw -->
<script src="{!! asset('assets/js/vendor/waw.js') !!}"></script>
<!-- aos js -->
<script src="{!! asset('assets/js/plugins/aos.js') !!}"></script>
<!-- jquery ui js -->
<script src="{!! asset('assets/js/plugins/jquery-ui.js') !!}"></script>
<!-- timepickers -->
<script src="{!! asset('assets/js/plugins/jquery-timepicker.js') !!}"></script>
<!-- sal animation -->
<script src="{!! asset('assets/js/vendor/sal.min.js') !!}"></script>
<!-- bootstrap JS -->
<script src="{!! asset('assets/js/plugins/bootstrap.min.js') !!}"></script>
<!-- easing JS -->
<script src="{!! asset('assets/js/plugins/jquery-slideNav.js') !!}"></script>
<!-- easing JS -->
<script src="{!! asset('assets/js/plugins/hover-revel.js') !!}"></script>
<!-- contact form js -->
<script src="{!! asset('assets/js/plugins/contact-form.js') !!}"></script>
<!-- main js -->
<script src="{!! asset('assets/js/main.js') !!}"></script>
<!-- swip image -->
<script src="{!! asset('assets/js/plugins/swip-img.js') !!}"></script>
<!-- header style two End -->


</body>

</html>
