<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{!! asset('assets/images/fav.png') !!}">
    <title>Works Construction</title>

    <!-- fontawesome css -->
    <link rel="stylesheet" href="{!! asset('assets/css/plugins/fontawesome-5.css') !!}">
    <!-- fontawesome css -->
    <link rel="stylesheet" href="{!! asset('assets/css/plugins/swiper.css') !!} ">
    <link rel="stylesheet" href="{!! asset('assets/css/plugins/aos.css') !!} ">
    <link rel="stylesheet" href="{!! asset('assets/css/plugins/unicons.css') !!} ">
    <link rel="stylesheet" href="{!! asset('assets/css/plugins/metismenu.css') !!} ">
    <link rel="stylesheet" href="{!! asset('assets/css/plugins/hover-revel.css') !!} ">
    <link rel="stylesheet" href="{!! asset('assets/css/plugins/timepickers.min.css') !!} ">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="{!! asset('assets/css/vendor/bootstrap.min.css') !!} ">
    <!-- main css -->
    <link rel="stylesheet" href="{!! asset('assets/css/style.css') !!} ">
</head>

<body>

<!-- header style two -->

<!-- header area start -->

<header class="header-one">
    <!-- hedaer top -->
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header-top-wrapper">
                        <!-- left area header top -->
                        <div class="left">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="13" height="17" viewBox="0 0 13 17" fill="none">
                                    <path d="M6.95455 6.95454H12.3636L5.40909 17V10.0455H0L6.95455 -3.8147e-06V6.95454Z" fill="#1875ba" />
                                </svg>
                            </div>
                            <p class="disc">
                                Nous passerons en revue toutes les étapes de la construction
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- hedaer bottom -->
    <!-- header style hear -->
    <div class="header-mid">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header-mid-one-wrapper">
                        <div class="logo-area">
                            <a href="index-2.html">
                                <img src="{!! asset('assets/images/logo/01.png') !!}" height="60px" width="60px" alt="logo">
                            </a>
                        </div>
                        <div class="header-right">
                            <!-- sinle map area -->
                            <div class="single-component info-con">
                                <div class="icon">
                                    <i class="fal fa-map-marker-alt"></i>
                                </div>
                                <div class="info">
                                    <span>Notre localisation</span>
                                    <a href="#">Yaoundé | Fougerolles Entrée Amity</a>
                                </div>
                            </div>
                            <!-- sinle map area -->
                            <!-- sinle map area -->
                            <div class="single-component info-con">
                                <div class="icon">
                                    <i class="fal fa-envelope"></i>
                                </div>
                                <div class="info">
                                    <span>Envoyez-nous un courrier</span>
                                    <a href="mailto:name@email.com">infos@works-construction.com</a>
                                </div>
                            </div>
                            <!-- sinle map area -->
                            <!-- sinle map area -->

                            <!-- sinle map area -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- header style hearm end -->

    <div class="nav-area-one  header--sticky">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="nav-header-area">
                        <div class="clip-path"></div>
                        <div class="wrapper">
                            <!-- header style two -->
                            <!-- nav area start -->
                            <div class="main-nav-desk nav-area">
                                <nav>
                                    <ul>
                                        <li class="menu-item">
                                            <a class="nav-item" href="{{ route('home') }}">Accueil</a>
                                        </li>
                                        <li class="has-droupdown pages">
                                            <a class="nav-link" href="#">Qui sommes-nous</a>
                                            <ul class="submenu inner-page">
                                                <li><a href="{{ route('index.php/about') }}">A propos de nous</a></li>
                                                <li><a href="{{ route('index.php/team') }}">Notre equipe</a></li>
                                                <li><a href="#">Chiffre d'affaire</a></li>
                                                <li><a href="#">Nos partenaires</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item">
                                            <a class="nav-item" href="{{ route('index.php/realisation') }}">
                                                Nos expériences
                                            </a>
                                        </li>
                                        <li class="menu-item">
                                            <a class="nav-item" href="{{ route('index.php/service') }}">Nos Services</a>
                                        </li>
                                        <li class="menu-item">
                                            <a class="nav-item" href="#">Contactez-nous</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <!-- nav-area end -->
                            <!-- header style two End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header area end -->

<!-- side bar for desktop -->
<div id="side-bar" class="side-bar header-two">
    <button class="close-icon-menu"><i class="far fa-times"></i></button>
    <!-- inner menu area desktop start -->
    <div class="inner-main-wrapper-desk">
        <div class="thumbnail">
            <img src="{!! asset('assets/images/banner/04.jpg') !!}" alt="elevate">
        </div>
        <div class="inner-content">
            <h4 class="title">We Build Building and Great Constructive Homes.</h4>
            <p class="disc">
                We successfully cope with tasks of varying complexity, provide long-term guarantees and regularly master new technologies.
            </p>
            <div class="footer">
                <h4 class="title">Got a project in mind?</h4>
                <a href="contact.html" class="rts-btn btn-seconday">Let's talk</a>
            </div>
        </div>
    </div>
    <!-- mobile menu area start -->
    <div class="mobile-menu d-block d-xl-none">
        <nav class="nav-main mainmenu-nav mt--30">
            <ul class="mainmenu metismenu" id="mobile-menu-active">
                <li class="has-droupdown">
                    <a href="#" class="main">Home</a>
                    <ul class="submenu mm-collapse">
                        <a href="#" class="tag">Multipage</a>
                        <li><a class="mobile-menu-link" href="index-2.html">Main Construction</a></li>
                        <li><a class="mobile-menu-link" href="index-two.html">Construction Home</a></li>
                        <li><a class="mobile-menu-link" href="index-three.html">Renovation Home</a></li>
                        <li><a class="mobile-menu-link" href="index-four.html">Construction Home</a></li>
                        <li><a class="mobile-menu-link" href="index-five.html">Construction Home</a></li>
                        <li><a class="mobile-menu-link" href="index-six.html">Renovation Home</a></li>
                        <li><a class="mobile-menu-link" href="index-seven.html">Engineering Home</a></li>
                        <li><a class="mobile-menu-link" href="index-eight.html">Handyman Home</a></li>
                        <li><a class="mobile-menu-link" href="index-nine.html">Industrial Home</a></li>
                    </ul>
                    <ul class="submenu mm-collapse">
                        <a href="#" class="tag">Onepage</a>
                        <li><a class="mobile-menu-link" href="onepage-one.html">Main Construction Onepage</a></li>
                        <li><a class="mobile-menu-link" href="onepage-two.html">Construction Onepage</a></li>
                        <li><a class="mobile-menu-link" href="onepage-three.html">Renovation Onepage</a></li>
                        <li><a class="mobile-menu-link" href="onepage-four.html">Construction Onepage</a></li>
                        <li><a class="mobile-menu-link" href="onepage-five.html">Construction Onepage</a></li>
                        <li><a class="mobile-menu-link" href="onepage-six.html">Renovation Onepage</a></li>
                        <li><a class="mobile-menu-link" href="onepage-eight.html">Handyman Onepage</a></li>
                        <li><a class="mobile-menu-link" href="onepage-seven.html">Engineering Onepage</a></li>
                        <li><a class="mobile-menu-link" href="onepage-nine.html">Industrial Onepage</a></li>
                    </ul>
                </li>
                <li class="has-droupdown">
                    <a href="#" class="main">Pages</a>
                    <ul class="submenu mm-collapse">
                        <li class="has-droupdown third-lvl">
                            <a class="main" href="#">Who We Are</a>
                            <ul class="submenu-third-lvl mm-collapse">
                                <li><a href="about.html"></a>About Us</li>
                                <li><a href="vision.html"></a>Vision</li>
                                <li><a href="careers.html"></a>Careers</li>
                                <li><a href="safety.html"></a>Safety</li>
                                <li><a href="sustainability.html"></a>Sustainability</li>
                            </ul>
                        </li>
                        <li><a class="mobile-menu-link" href="company-story.html">Our History</a></li>
                        <li><a class="mobile-menu-link" href="team.html">Team</a></li>
                        <li><a class="mobile-menu-link" href="team-details.html">Team Details</a></li>
                        <li><a class="mobile-menu-link" href="appoinment.html">Appoinment</a></li>
                        <li><a class="mobile-menu-link" href="404.html">Error 404</a></li>
                        <li class="has-droupdown third-lvl">
                            <a class="main" href="#">Shop</a>
                            <ul class="submenu-third-lvl mm-collapse">
                                <li><a href="shop.html"></a>Shop</li>
                                <li><a href="single-product.html"></a>Single Product</li>
                                <li><a href="cart.html"></a>Cart</li>
                                <li><a href="checkout.html"></a>Checkout</li>
                                <li><a href="account.html"></a>Account</li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="has-droupdown">
                    <a href="#" class="main">Services</a>
                    <ul class="submenu mm-collapse">
                        <li><a class="mobile-menu-link" href="service.html">Service</a></li>
                        <li><a class="mobile-menu-link" href="service-details.html">Service Details</a></li>
                    </ul>
                </li>
                <li class="has-droupdown">
                    <a href="#" class="main">Portfolio</a>
                    <ul class="submenu mm-collapse">
                        <li><a class="mobile-menu-link" href="project.html">Project</a></li>
                        <li><a class="mobile-menu-link" href="project-details.html">Project Details</a></li>
                    </ul>
                </li>
                <li class="has-droupdown">
                    <a href="#" class="main">Blog</a>
                    <ul class="submenu mm-collapse">
                        <li><a class="mobile-menu-link" href="blog-list.html">Blog List</a></li>
                        <li><a class="mobile-menu-link" href="blog-grid.html">Blog Grid</a></li>
                        <li><a class="mobile-menu-link" href="blog-details.html">Blog Details</a></li>
                    </ul>
                </li>
                <li>
                    <a href="contactus.html" class="main">Contact Us</a>
                </li>
            </ul>
        </nav>

        <div class="social-wrapper-one">
            <ul>
                <li>
                    <a href="#">
                        <i class="fa-brands fa-facebook-f"></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa-brands fa-twitter"></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa-brands fa-youtube"></i>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa-brands fa-linkedin-in"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- mobile menu area end -->
</div>
<!-- header style two End -->

<!-- Bread-Crumb style two -->
<!-- rts breadcrumba area start -->
<div class="rts-bread-crumb-area ptb--150 ptb_sm--100 bg-breadcrumb bg_image">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- bread crumb inner wrapper -->
                <div class="breadcrumb-inner text-center">
                    <h1 class="title">Notre Contact</h1>
                    <div class="meta">
                        <a href="#" class="prev">Accueil /</a>
                        <a href="#" class="next">Notre Contact</a>
                    </div>
                </div>
                <!-- bread crumb inner wrapper end -->
            </div>
        </div>
    </div>
</div>


{{--<div class="rts-section-gap3 team-3bg">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-lg-4 col-md-6 col-sm-12 col-12">--}}
{{--                <!-- single team one start -->--}}
{{--                <div class="single-team-three">--}}
{{--                    <div class="thumbnail">--}}
{{--                        <img src="{!! asset('assets/images/team/03.jpg') !!}" alt="team_area-start">--}}
{{--                    </div>--}}
{{--                    <div class="content-area">--}}
{{--                            <span class="designation">--}}
{{--                                Président directeur Général--}}
{{--                            </span>--}}
{{--                        <a href="team-details.html">--}}
{{--                            <h6 class="name">DANIEL SMITH</h6>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!-- single team one start -->--}}
{{--            </div>--}}
{{--            <div class="col-lg-4 col-md-6 col-sm-12 col-12">--}}
{{--                <!-- single team one start -->--}}
{{--                <div class="single-team-three">--}}
{{--                    <div class="thumbnail">--}}
{{--                        <img src="{!! asset('assets/images/team/03.jpg') !!}" alt="team_area-start">--}}
{{--                    </div>--}}
{{--                    <div class="content-area">--}}
{{--                            <span class="designation">--}}
{{--                               Directeur Général--}}
{{--                            </span>--}}
{{--                        <a href="team-details.html">--}}
{{--                            <h6 class="name">William James</h6>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!-- single team one start -->--}}
{{--            </div>--}}
{{--            <div class="col-lg-4 col-md-6 col-sm-12 col-12">--}}
{{--                <!-- single team one start -->--}}
{{--                <div class="single-team-three">--}}
{{--                    <div class="thumbnail">--}}
{{--                        <img src="{!! asset('assets/images/team/03.jpg') !!}" alt="team_area-start">--}}
{{--                    </div>--}}
{{--                    <div class="content-area">--}}
{{--                            <span class="designation">--}}
{{--                                Directeur des Resources Humaine--}}
{{--                            </span>--}}
{{--                        <a href="team-details.html">--}}
{{--                            <h6 class="name">Theodore Henry</h6>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!-- single team one start -->--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="row mt--50">--}}
{{--            <div class="col-lg-4 col-md-6 col-sm-12 col-12">--}}
{{--                <!-- single team one start -->--}}
{{--                <div class="single-team-three">--}}
{{--                    <div class="thumbnail">--}}
{{--                        <img src="{!! asset('assets/images/team/03.jpg') !!}" alt="team_area-start">--}}
{{--                    </div>--}}
{{--                    <div class="content-area">--}}
{{--                            <span class="designation">--}}
{{--                               Comptable Général--}}
{{--                            </span>--}}
{{--                        <a href="team-details.html">--}}
{{--                            <h6 class="name">Camilo Gillian</h6>--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <!-- single team one start -->--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

<div class="rts-section-gap3 team-3bg">
        <div class="rts-contact-area-m rts-section-gap">
            <div class="container">
                <div class="row g-24">
                    <!-- single contact area -->
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="single-contact-one-inner">
                            <div class="thumbnail">
                                <img src="assets/images/contact/01.jpg" alt="">
                            </div>
                            <div class="content">
                                <div class="icone">
                                    <img src="assets/images/contact/shape/01.svg" alt="">
                                </div>
                                <div class="info">
{{--                                    <span>Call Us 24/7</span>--}}
                                    <a href="tel:+18475555555">
                                        <h5>237 697 07 33 58 | +1 825 747 0819</h5>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single contact area end -->
                    <!-- single contact area -->
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="single-contact-one-inner">
                            <div class="thumbnail">
                                <img src="assets/images/contact/01.jpg" alt="">
                            </div>
                            <div class="content">
                                <div class="icone">
                                    <img src="assets/images/contact/shape/02.svg" alt="">
                                </div>
                                <div class="info">
                                    <span>	Adresse mail </span>
                                    <a href="mailto:works.construction20@gmail.com">
                                        <h5>Works.construction20@gmail.com</h5>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single contact area end -->
                    <!-- single contact area -->
                    <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
                        <div class="single-contact-one-inner">
                            <div class="thumbnail">
                                <img src="assets/images/contact/01.jpg" alt="">
                            </div>
                            <div class="content">
                                <div class="icone">
                                    <img src="assets/images/contact/shape/03.svg" alt="">
                                </div>
                                <div class="info">
                                    <span>Localisation : Yaoundé | Hippodrome-Avenue des banques</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- single contact area end -->
                </div>
            </div>
        </div>
    </div>


<!-- rts footer area start -->
<div class="rts-footer-area footer-one footer-bg bg_image">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- footer top -->
                <div class="footer-top-wrapper  ptb--80">
                    <a href="#" class="logo-area" data-sal="zoom-in" data-sal-delay="150" data-sal-duration="800">
                        <img src="{!! asset('assets/images/logo/01.png') !!}" height="60px" width="60px" alt="blog-images">
                    </a>
                    <h4 class="title" data-sal="zoom-in" data-sal-delay="150" data-sal-duration="800">
                        INSCRIVEZ-VOUS À NOTRE LETTRE
                        <br>
                        D'INFORMATION POUR LES DERNIÈRES MISES À JOUR
                    </h4>
                    <div class="subscribe-area" data-sal="zoom-in" data-sal-delay="150" data-sal-duration="800">
                        <form>
                            <input type="email" name="email" placeholder="Email Address" required>
                            <button class="rts-btn btn-primary">S'abonner</button>
                        </form>
                    </div>
                </div>
                <!-- footer top end -->
            </div>
        </div>
    </div>
    <div class="copyright-area-one">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="wrapepr">
                        <p>Copyright 2023 works construction. Tous droits réservés.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- rts footer area end -->
<!-- Footer two End -->

<!-- header two -->

<!-- progress area start -->
<div class="progress-wrap active-progress">
    <svg class="progress-circle svg-content" width="100%" height="100%" viewBox="-1 -1 102 102">
        <path d="M50,1 a49,49 0 0,1 0,98 a49,49 0 0,1 0,-98" style="transition: stroke-dashoffset 10ms linear 0s; stroke-dasharray: 307.919, 307.919; stroke-dashoffset: 172.889;"></path>
    </svg>
</div>
<!-- progress area end -->

<!-- cart area edn -->

<div class="search-input-area">
    <div class="container">
        <div class="search-input-inner">
            <div class="input-div">
                <input id="searchInput1" class="search-input" type="text" placeholder="Search by keyword or #">
                <button><i class="far fa-search"></i></button>
            </div>
        </div>
    </div>
    <div id="close" class="search-close-icon"><i class="far fa-times"></i></div>
</div>


<div id="anywhere-home" class="">
</div>

<!-- pre loader start -->
<div id="elevate-load" class="loaded">
    <div class="loader-wrapper">
        <div class="lds-ellipsis">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>
</div>
<!-- pre loader end -->



<!-- jquery js -->
<script src="{!! asset('assets/js/plugins/jquery.min.js') !!}"></script>
<!-- jquery ui -->
<script src="{!! asset('assets/js/vendor/jqueryui.js') !!}"></script>
<!-- counter up -->
<script src="{!! asset('assets/js/plugins/counter-up.js') !!}"></script>
<script src="{!! asset('assets/js/plugins/swiper.js') !!}"></script>
<!-- twinmax -->
<script src="{!! asset('assets/js/vendor/twinmax.js') !!}"></script>
<!-- split text js -->
<script src="{!! asset('assets/js/vendor/split-text.js') !!}"></script>
<!-- text plugins -->
<script src="{!! asset('assets/js/plugins/text-plugins.js') !!}"></script>
<!-- metismenu js -->
<script src="{!! asset('assets/js/plugins/metismenu.js') !!}"></script>
<!-- waypoint js -->
<script src="{!! asset('assets/js/vendor/waypoint.js') !!}"></script>
<!-- waw -->
<script src="{!! asset('assets/js/vendor/waw.js') !!}"></script>
<!-- aos js -->
<script src="{!! asset('assets/js/plugins/aos.js') !!}"></script>
<!-- jquery ui js -->
<script src="{!! asset('assets/js/plugins/jquery-ui.js') !!}"></script>
<!-- timepickers -->
<script src="{!! asset('assets/js/plugins/jquery-timepicker.js') !!}"></script>
<!-- sal animation -->
<script src="{!! asset('assets/js/vendor/sal.min.js') !!}"></script>
<!-- bootstrap JS -->
<script src="{!! asset('assets/js/plugins/bootstrap.min.js') !!}"></script>
<!-- easing JS -->
<script src="{!! asset('assets/js/plugins/jquery-slideNav.js') !!}"></script>
<!-- easing JS -->
<script src="{!! asset('assets/js/plugins/hover-revel.js') !!}"></script>
<!-- contact form js -->
<script src="{!! asset('assets/js/plugins/contact-form.js') !!}"></script>
<!-- main js -->
<script src="{!! asset('assets/js/main.js') !!}"></script>
<!-- swip image -->
<script src="{!! asset('assets/js/plugins/swip-img.js') !!}"></script>
<!-- header style two End -->

</body>

</html>
